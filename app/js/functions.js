var is_mobile = function(){
  var widths = [320, 360];
  for (var i in widths){
    if (Modernizr.mq('all and (max-width: '+widths[i]+'px)')) {
      return true;
    }
  }
  return false;
}