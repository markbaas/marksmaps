'use strict';

/* Filters */

var app = angular.module('myApp.filters', []);
  
app.filter('interpolate', ['version', function(version) {
  return function(text) {
    return String(text).replace(/\%VERSION\%/mg, version);
  };

}]);

app.filter('distance', function () {
  return function (input) {
    if (input >= 1000) {
      return (input/1000).toFixed(1) + 'km';
    } else {
      return input + 'm';
    }
  } 
});