'use strict';

/* Directives */


var directives = angular.module('myApp.directives', []);

directives.directive('resizable', function($window) {
  return function($scope, elm, attrs) {
    // get parent
    var parent = elm.parent()[0];

    // resize method
    $scope.initializeWindowSize = function() {
      elm.css("height", parent.offsetHeight + "px");
      //elm.css("width", "inherit");
    };
    $scope.initializeWindowSize();
    console.log($window);
    // bind to element
    return angular.element($window).bind('resize', function() {
      $scope.initializeWindowSize();
      return $scope.$apply();
    });
  };
});

directives.directive('fillHeight', function($window) {
  return function($scope, elm, attrs) {
    // get parent
    var parent = elm.parent();
    var style = window.getComputedStyle(parent[0]);
    var paddingParent = parseFloat(style.paddingTop) + parseFloat(style.paddingBottom);

    // resize method
    $scope.initializeWindowSize = function() {
      var children = parent.children()
      var height = 0;
      for (var i = 0; i < children.length; i++){
        var child = children[i];
        var style = window.getComputedStyle(child);
        var margin = parseFloat(style.marginTop) + parseFloat(style.marginBottom);
        if (child !== elm[0]){
          height += (child.offsetHeight + margin);
        } else {
          height += margin;
        }
      }
      elm.css("height", (parent[0].offsetHeight - height - paddingParent) + "px");
    };
    $scope.initializeWindowSize();

    // bind to element
    return angular.element($window).bind('resize', function() {
      $scope.initializeWindowSize();
      return $scope.$apply();
    });
  };
});


directives.directive('appVersion', ['version', function(version) {
  return function($scope, elm, attrs) {
    elm.text(version);
  };
}]);

directives.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

directives.directive("compileHtml", function($parse, $sce, $compile) {
    return {
        restrict: "A",
        link: function (scope, element, attributes) {
 
            var expression = $sce.parseAsHtml(attributes.compileHtml);
 
            var getResult = function () {
                return expression(scope);
            };
 
            scope.$watch(getResult, function (newValue) {
                if (!newValue) return;
                var linker = $compile(newValue);
                element.empty();
                element.append(linker(scope.$parent));
            });
        }
    }
});

directives.directive('itineraryTable', function($window) {
  return function($scope, elm, attrs){
    var routes = $scope.routeLayer._routes;
    for (var i in routes) {
      var alt = routes[i];
      if (alt.name === attrs.name){
        switch (instr.type) {
          case 'Straight':
            return (i === 0 ? 'Head' : 'Continue') + ' {dir}' + (instr.road ? ' on {road}' : '');
          case 'SlightRight':
            return 'Slight right' + (instr.road ? ' onto {road}' : '');
          case 'Right':
            return 'Right' + (instr.road ? ' onto {road}' : '');
          case 'SharpRight':
            return 'Sharp right' + (instr.road ? ' onto {road}' : '');
          case 'TurnAround':
              return 'Turn around';
          case 'SharpLeft':
            return 'Sharp left' + (instr.road ? ' onto {road}' : '');
          case 'Left':
            return 'Left' + (instr.road ? ' onto {road}' : '');
          case 'SlightLeft':
            return 'Slight left' + (instr.road ? ' onto {road}' : '');
          case 'WaypointReached':
            return 'Waypoint reached';
          case 'Roundabout':
            return 'Take the {exit} exit in the roundabout';
          case 'DestinationReached':
            return 'Destination reached';
		}
          
        
        var table = $scope.routeLayer._createItineraryTable(alt)
        elm.html(table.outerText); 
        return;
      }
    }
  }
});