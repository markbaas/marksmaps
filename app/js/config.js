var mapTiles = {
  mapquestosm: {
    name: "MapQuest Open",
    url: "http://otile1.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpg",
    type: "xyz"
  },
  openstreetmap: {
    name: "OpenStreetMaps",
    url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    type: "xyz",
  },
  opencyclemap: {
    name: "OpenCycleMap",
    url: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
    type: "xyz",
  },
  publictransport: {
    name: "Public Transport",
    url: "http://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png",
    type: "xyz"
  }
};

var mapOverlays = {
  panoramio: {
    name: "Photos",
    type: "custom",
    layer: new L.Panoramio({maxLoad: 50, maxTotal: 250})
  }
};
      