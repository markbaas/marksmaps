'use strict';

/* Controllers */

var app = angular.module('myApp.controllers', ['leaflet-directive']);

app.controller('MapsCtrl', function($scope, $sce, $http, $modal, $templateCache, $window, leafletData) {
  // vars
  $scope.directions = {to: "Veenendaal", from: "Eindhoven"};
  $scope.itinerary = { content: '', visible: true};
  $scope.transportation = 'car';
  $scope.searchKeywords = "";
  $scope.panel = { 'visible': false };
  $scope.directionsValid = {'from': true, 'to': true};
  $scope.routeLayer = null;
  $scope.routes = [];
  $scope.is_mobile = is_mobile();
  $scope.config = {mapTiles: mapTiles, mapOverlays: mapOverlays, layerSelected: "mapquestosm"}

  // Map settings
  var zoomControl = ($scope.is_mobile) ? false : true;
  var layerControlPosition = 'bottomright';
  var baselayers = {}
  baselayers[$scope.config.layerSelected] = mapTiles[$scope.config.layerSelected];
  
  angular.extend($scope, {
    defaults: {
      zoomControl: zoomControl,
    },
    center: {
      lat: -17.389958246585554,
      lng: -66.13374710083008,
      zoom: 8
    },
    layers: {
      baselayers: baselayers
    }
  });

  // do search
  $scope.doSearch = function(keywords){
    $scope.getGeoLocation(keywords, function(loc){
      leafletData.getMap().then(function(map){
        map.panTo([loc.lat, loc.lon]);
        map.fitBounds([
            [loc.boundingbox[0], loc.boundingbox[2]],
            [loc.boundingbox[1], loc.boundingbox[3]]
        ]);
      });
    });
  };
  
  // show panel
  $scope.togglePanel = function(show){
    if (show === null || show === undefined){
      $scope.panel.visible = !$scope.panel.visible;
    } else {
      $scope.panel.visible = show;
    }  
  };
  
  // geolocation
  $scope.getGeoLocation = function(keywords, success, error) {
    $http.get("http://nominatim.openstreetmap.org/search/?q="+keywords+"&format=json")
    .success(function(data, status, headers, config) {
      if (data.length > 0){
        var loc = data[0];
        console.log(keywords, loc);
        success(loc);
      } else {
        if (error) error(keywords);
      }
    }).error(function(data, status, headers, config){
      alert("error has occured");
    });
  };

  // get geolocation
  $scope.getLocation = function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(pos){
          leafletData.getMap().then(function(map){
            map.panTo([pos.coords.latitude, pos.coords.longitude]);
            map.setZoom(16);
          });
        });
    } else {
        alert("Please enable GPS");
    }
  };
  
  // get directions
  $scope.getDirections = function(){
    leafletData.getMap().then(function(map){
      $scope.getGeoLocation($scope.directions.from, function(geoFrom){
        // set valid from
        $scope.directionsValid.from = true;
        
        $scope.getGeoLocation($scope.directions.to, function(geoTo){
          // set valid to
          $scope.directionsValid.to = true;
          
          // clean old layer if any
          $scope.cleanRouteLayer(map);
          
          // create layer
          $scope.routeLayer = L.Routing.control({
            waypoints: [
                L.latLng(geoFrom.lat, geoFrom.lon),
                L.latLng(geoTo.lat, geoTo.lon)
            ],
            show: false
          });
          
          // add to map
          $scope.routeLayer.addTo(map).on('routesfound', function(e){
            $scope.routes = e.routes;
            // render the template
            $http.get('partials/templates/itinerary.html', { cache: $templateCache })
            .then(function(response) {
              // set content
              $scope.itinerary.content = $sce.trustAsHtml(response.data);
              
              if ($scope.is_mobile) $scope.itinerary.visible = false;
            });
          });
          
          // Hide dialog
          if ($scope.is_mobile) {
            $scope.togglePanel(false);
          }
        }, function(keywords){
          $scope.directionsValid.to = false;
        });
      }, function(keywords){
        $scope.directionsValid.from = false;
      });
    });
  };     
  
  // Set alternative
  $scope.setAlternative = function(name){
    var routes = $scope.routeLayer._routes;
    for (var i in routes) {
      var alt = routes[i];
      if (alt.name === name){
        $scope.routeLayer.fire('routeselected', {route: alt});
        return;
      }
    }
  };
  
  // Show error dialog
  $scope.showError = function(message){
    var modalInstance = $modal.open({
      templateUrl: 'partials/templates/errordialog.html',
      controller: 'ErrorDialogCtrl',
      resolve: {
        params: function() { return {
          title: 'Error!',
          message: message
        };}
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
  
  $scope.toggleItinerary = function(show){
    if (show === null || show === undefined){
      $scope.itinerary.visible = !$scope.itinerary.visible;
    } else {
      $scope.itinerary.visible = show;
    }  
    leafletData.getMap().then(function(map){
      //map.focus();
    });
  }
  
  $scope.dragItinerary = function(data){
    var elem = document.getElementsByClassName('itinerary')[0];
    if (data.isFinal) {
      
      if (data.deltaY < -50 && $scope.itinerary.visible === false) {
        $scope.toggleItinerary(true);
      } else if (data.deltaY > 50 && $scope.itinerary.visible === true){
        $scope.toggleItinerary(false);
      } 
      elem.removeAttribute("style");
    } else {
      elem.style.transform = "translateY(" + data.deltaY + "px)";
    }
  }
  
  $scope.cleanRouteLayer = function(map){
    if ($scope.routeLayer) {
      $scope.routeLayer.onRemove(map);
      $scope.itinerary.content = "<span></span>";
    } 
  };
  
  $scope.clearDirections = function(){
    leafletData.getMap().then(function(map){
      $scope.cleanRouteLayer(map);
    });
  };
  
  $scope.showLayers = function() {
    $scope.layersPanel.visible = !$scope.layersPanel.visible;
  }
  
  // Watches
  $scope.$watch('config.layerSelected', function(){
    var layerId = $scope.config.layerSelected;
    console.log(layerId);
    var baselayers = $scope.layers.baselayers;
    for (var i in baselayers){
      delete $scope.layers.baselayers[i];
    }
    baselayers[layerId] = $scope.config.mapTiles[layerId];
  });
  // END MAPS CONTROLLER
});

app.controller('ErrorDialogCtrl', function($scope, $modalInstance, params) {
  $scope.message = params.message;
  $scope.title = params.title;
  
  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});