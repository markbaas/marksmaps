module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
            options: { 
              loadPath: ['./app/bower_components/foundation/scss'],
              debugInfo: true
            },
			dist: {
				files: [{
					expand: true,
					cwd: './app/scss',
					src: ['*.scss'],
					dest: './app/css',
					ext: '.css'
				}]
			}
		},
		devserver: {server: {}},
        watch: {
              css: {
                  files: ['./app/scss/*.scss'],
                  tasks: ['sass'],
              } 
          },
	});
	
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-devserver');
    grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('start', ['devserver']);

};